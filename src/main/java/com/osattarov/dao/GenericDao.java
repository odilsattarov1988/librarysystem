package com.osattarov.dao;

import java.util.List;

/**
 * Created by Odiljon Sattarov on 4/7/2015.
 */
public interface GenericDao<T, PK> {
    List<T> findAll(Class<T> entityClass);

    T findById(PK id, Class<T> entityClass);

    T create(T t);

    T update(T t);

    void delete(T t);
    
    T load(PK id, Class<T> entityClass);
}
