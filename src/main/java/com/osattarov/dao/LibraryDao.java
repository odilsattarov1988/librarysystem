/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.osattarov.dao;

import com.osattarov.entities.Copy;
import com.osattarov.entities.Loan;
import com.osattarov.entities.Reservation;

/**
 *
 * @author Odiljon Sattarov
 */
public interface LibraryDao {

    public void reserveOrder(Long memberId, Long publicationId);

    public void cancelReservation(Reservation reservation);

    public void pickupReservation(Reservation reservation);

    public Loan getLoan(Long memberId, Long copyId);

    public void returnLoan(Loan loan);

    public Reservation hold(Long memberId, Long reservationId);

    public Copy createCopy(Copy copy);
}
