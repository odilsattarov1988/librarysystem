/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.osattarov.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author Odiljon Sattarov
 */
public class Utilities {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public static Calendar getCurrentDate() {
        return Calendar.getInstance();
    }

    public static String formatDate(Calendar cal) {
        return sdf.format(cal.getTime());
    }

    public static GregorianCalendar clone(GregorianCalendar calendar) {
        return new GregorianCalendar(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }
}
