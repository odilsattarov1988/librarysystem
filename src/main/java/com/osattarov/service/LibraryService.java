package com.osattarov.service;

import com.osattarov.entities.Copy;
import com.osattarov.entities.Loan;
import com.osattarov.entities.Reservation;

/**
 * Created by Odiljon Sattarov on 4/7/2015.
 *
 */
public interface LibraryService {

    public void reserveOrder(Long memberId, Long publicationId);

    public void cancelReservation(Reservation reservation);

    public void pickupReservation(Reservation reservation);

    public Loan getLoan(Long memberId, Long copyId);

    public void returnLoan(Loan loan);

    public Reservation hold(Long memberId, Long reservationId);
    
    public Copy createCopy(Copy copy);
}
