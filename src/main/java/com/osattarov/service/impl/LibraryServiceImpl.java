package com.osattarov.service.impl;

import com.osattarov.dao.GenericDao;
import com.osattarov.dao.LibraryDao;
import com.osattarov.entities.Copy;
import com.osattarov.entities.Loan;
import com.osattarov.entities.Member;
import com.osattarov.entities.Publication;
import com.osattarov.entities.Reservation;
import com.osattarov.entities.StatusType;
import com.osattarov.service.LibraryService;
import com.osattarov.util.Utilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Odiljon Sattarov on 4/7/2015.
 */
@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class LibraryServiceImpl implements LibraryService {

    private GenericDao<Member, Long> memberDao;
    private GenericDao<Publication, Long> publicationDao;
    private GenericDao<Loan, Long> loanDao;
    private GenericDao<Reservation, Long> reservationDao;
    private GenericDao<Copy, Long> copyDao;
    @Autowired
    private LibraryDao libraryDao;

    @Override
    public void reserveOrder(Long memberId, Long publicationId) {
        Member member = memberDao.findById(memberId, Member.class);
        Publication publication = publicationDao.findById(publicationId, Publication.class);
        Reservation reservation = new Reservation(member, publication, Utilities.getCurrentDate());
        reservationDao.create(reservation);
    }

    @Override
    public void cancelReservation(Reservation reservation) {
        reservation.setStatusType(StatusType.CANCELLED);
        reservation.setStatusDate(Utilities.getCurrentDate());
        if (reservation.getCopy() != null) {
            reservation.getCopy().setStatus(StatusType.AVAILABLE);
        }

        reservationDao.update(reservation);
    }

    @Override
    public void pickupReservation(Reservation reservation) {
        reservation.getPublication().getReservations().remove(reservation);
        reservation.getMember().getReservations().remove(reservation);
        reservation.getCopy().getReservations().remove(reservation);

        Loan loan = new Loan(reservation.getMember(), reservation.getCopy(), Utilities.getCurrentDate());
        loanDao.create(loan);

    }

    @Override
    public Loan getLoan(Long memberId, Long copyId) {
        Member member = memberDao.findById(memberId, Member.class);
        Copy copy = copyDao.findById(copyId, Copy.class);
        Loan loan = new Loan(member, copy, Utilities.getCurrentDate());
        loanDao.create(loan);

        return loan;
    }

    @Override
    public void returnLoan(Loan loan) {
        Copy copy = loan.getCopy();
        Publication publication = copy.getPublication();
        loan.setReturnDate(Utilities.getCurrentDate());

        //Qilinishi kerak bo'lgan ishlar bor
        loanDao.update(loan);
    }

    @Override
    public Reservation hold(Long memberId, Long reservationId) {
        Member member = memberDao.findById(memberId, Member.class);
        Reservation reservation = reservationDao.findById(reservationId, Reservation.class);

        //Qilinishi kerak bo'lgan ishlar bor
        return reservation;
    }

    @Override
    public Copy createCopy(Copy copy) {
        return libraryDao.createCopy(copy);
    }
}
