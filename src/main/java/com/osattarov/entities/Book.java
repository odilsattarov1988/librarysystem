package com.osattarov.entities;

import javax.persistence.*;
import java.util.List;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by Odiljon Sattarov on 4/7/2015.
 */
@Entity
public class Book extends Publication {

    @NotEmpty
    @Column(nullable = false)
    private String ISBN;
    @ManyToMany
    @JoinTable(name = "BOOK_AUTHOR", joinColumns = @JoinColumn(name = "BOOK_ID"), inverseJoinColumns = @JoinColumn(name = "AUTHOR_ID"))
    private List<Author> authors;

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }
}
