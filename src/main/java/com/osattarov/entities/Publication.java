package com.osattarov.entities;

import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.Min;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by Odiljon Sattarov on 4/7/2015.
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Publication {

    @Id
    @GeneratedValue
    private long id;
    @NotEmpty
    private String title;
    @Min(1)
    @Column(name = "MAX_CHECKOUT_LENGTH")
    private long maxCheckoutLength;
    @OneToMany(mappedBy = "publication", fetch = FetchType.EAGER)
    private List<Reservation> reservations;
    @OneToMany(mappedBy = "publication", fetch = FetchType.EAGER)
    @org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.DELETE)
    private List<Copy> copies;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getMaxCheckoutLength() {
        return maxCheckoutLength;
    }

    public void setMaxCheckoutLength(long maxCheckoutLength) {
        this.maxCheckoutLength = maxCheckoutLength;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public List<Copy> getCopies() {
        return copies;
    }

    public void setCopies(List<Copy> copies) {
        this.copies = copies;
    }

    public long getAvailableCopies() {
        long result = 0;
        for (Copy c : getCopies()) {
            if (c.getStatus() == StatusType.AVAILABLE) {
                result++;
            }
        }
        return result;
    }

    public long getPendingReservation() {
        long result = 0;
        for (Reservation r : getReservations()) {
            if (r.getStatusType() == StatusType.PENDING) {
                result++;
            }
        }
        return result;
    }

    public long getOnHoldReservation() {
        long result = 0;
        for (Reservation r : getReservations()) {
            if (r.getStatusType() == StatusType.ON_HOLD) {
                result++;
            }
        }
        return result;
    }

    public Reservation getEarliestReservation() {
        Reservation result = null;
        for (Reservation reservation : reservations) {
            reservation.updateReservationIfExpired();
            if (reservation.getStatusType() == StatusType.PENDING) {
                result = reservation;
                break;
            }
        }
        return result;
    }

    public boolean hasPendingReservation() {
        for (Reservation r : getReservations()) {
            r.updateReservationIfExpired();
        }
        return !getReservations().isEmpty();
    }
}
