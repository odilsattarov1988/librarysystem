package com.osattarov.entities;

import com.osattarov.util.Utilities;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.persistence.*;

/**
 * Created by Odiljon Sattarov on 4/7/2015.
 */
@Entity
public class Reservation {

    @Id
    @GeneratedValue
    private long id;
    @Column(name = "STATUS_DATE")
    private Calendar statusDate;
    @Column(name = "STATUS_TYPE")
    private StatusType statusType;
    @ManyToOne
    @JoinColumn(name = "MEMBER_ID")
    private Member member;
    @ManyToOne
    private Publication publication;
    @ManyToOne
    private Copy copy;

    public Reservation() {
    }

    public Reservation(Member member, Publication publication, Calendar statusDate) {
        this.statusDate = statusDate;
        this.member = member;
        this.publication = publication;
        this.statusType = StatusType.PENDING;
        addReservationToPublication();
        addReservationToMember();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Calendar getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Calendar statusDate) {
        this.statusDate = statusDate;
    }

    public StatusType getStatusType() {
        return statusType;
    }

    public void setStatusType(StatusType statusType) {
        this.statusType = statusType;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Publication getPublication() {
        return publication;
    }

    public void setPublication(Publication publication) {
        this.publication = publication;
    }

    public Copy getCopy() {
        return copy;
    }

    public void setCopy(Copy copy) {
        this.copy = copy;
    }

    private void addReservationToPublication() {
        getPublication().getReservations().add(this);
    }

    private void addReservationToMember() {
        getMember().getReservations().add(this);
    }

    public void updateReservationIfExpired() {
        if (StatusType.ON_HOLD == statusType) {
            Calendar temp =Utilities.clone((GregorianCalendar) getStatusDate());
            temp.add(Calendar.DAY_OF_MONTH, 7);
            if(Utilities.getCurrentDate().compareTo(temp) > 0) {
                cancel();
                if (getCopy().getPublication().hasPendingReservation()) {
                    Reservation r = getCopy().getPublication().getEarliestReservation();
                    getCopy().hold(r.getMember(), r);
                }
            }
        }
    }

    public void cancel() {
        setStatusType(StatusType.CANCELLED);
        setStatusDate(Utilities.getCurrentDate());
        if (copy != null) {
            copy.setStatus(StatusType.AVAILABLE);
        }
    }
}
