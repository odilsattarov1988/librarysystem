package com.osattarov.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by Odiljon Sattarov on 4/7/2015.
 */
@Entity
public class Magazine extends Publication {
    @Column(name = "ISSUE_NUMBER", nullable = false)
    @NotEmpty
    private String issueNumber;

    public String getIssueNumber() {
        return issueNumber;
    }

    public void setIssueNumber(String issueNumber) {
        this.issueNumber = issueNumber;
    }
}
