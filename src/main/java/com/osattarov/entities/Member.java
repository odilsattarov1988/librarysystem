package com.osattarov.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;
import javax.persistence.FetchType;
import org.hibernate.annotations.NotFound;

/**
 * Created by Odiljon Sattarov on 4/7/2015.
 */
@Entity
public class Member extends Person {
    @Column(name = "MEMBER_NUMBER", nullable = false)
    private long memberNumber;
    @OneToMany(mappedBy = "member",fetch = FetchType.EAGER)
    @NotFound
    private List<Reservation> reservations;
    @OneToMany(mappedBy = "member", fetch = FetchType.EAGER)
    @NotFound
    private List<Loan> loans;

    public long getMemberNumber() {
        return memberNumber;
    }

    public void setMemberNumber(long memberNumber) {
        this.memberNumber = memberNumber;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public List<Loan> getLoans() {
        return loans;
    }

    public void setLoans(List<Loan> loans) {
        this.loans = loans;
    }
}
