package com.osattarov.entities;

import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by Odiljon Sattarov on 4/7/2015.
 */
@Entity
public class Loan {

    @Id
    @GeneratedValue
    private long id;
    @Column(name = "CHECKOUT_DATE")
    private Calendar checkoutDate;
    @Column(name = "RETURN_DATE")
    private Calendar returnDate;
    @ManyToOne
    @JoinColumn(name = "MEMBER_ID")
    private Member member;
    @ManyToOne
    private Copy copy;

    public Loan() {
    }

    public Loan(Member member, Copy copy, Calendar checkoutDate) {
        this.member = member;
        this.copy = copy;
        this.checkoutDate = checkoutDate;
        addLoanToCopy();
        addLoanToMember();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Calendar getCheckoutDate() {
        return checkoutDate;
    }

    public void setCheckoutDate(Calendar checkoutDate) {
        this.checkoutDate = checkoutDate;
    }

    public Calendar getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Calendar returnDate) {
        this.returnDate = returnDate;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Copy getCopy() {
        return copy;
    }

    public void setCopy(Copy copy) {
        this.copy = copy;
    }

    private void addLoanToCopy() {
        getCopy().addLoan(this);
    }

    private void addLoanToMember() {
        getMember().getLoans().add(this);
    }
}
