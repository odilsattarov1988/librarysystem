package com.osattarov.entities;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Created by Odiljon Sattarov on 4/7/2015.
 */
@Entity
public class Copy {

    @Id
    @GeneratedValue
    private long id;
    private StatusType status;
    @OneToMany(mappedBy = "copy")
    private List<Loan> loans;
    @OneToMany(mappedBy = "copy")
    private List<Reservation> reservations;
    @ManyToOne
    private Publication publication;

    public Copy() {
    }

    public Copy(Publication publication) {
        this.publication = publication;

        if (!publication.hasPendingReservation()) {
            status = StatusType.AVAILABLE;
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public StatusType getStatus() {
        return status;
    }

    public void setStatus(StatusType status) {
        this.status = status;
    }

    public List<Loan> getLoans() {
        return loans;
    }

    public void setLoans(List<Loan> loans) {
        this.loans = loans;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public Publication getPublication() {
        return publication;
    }

    public void setPublication(Publication publication) {
        this.publication = publication;
    }

    public void addLoan(Loan loan) {
        loans.add(loan);
        setStatus(StatusType.LOANED);
    }

    public Reservation hold(Member member, Reservation reservation) {
        reservation.setCopy(this);
        reservation.setStatusType(StatusType.ON_HOLD);
        setStatus(StatusType.ON_HOLD);
        getReservations().add(reservation);
        return reservation;
    }

    public String getMemberName() {
        String name = "";
        if (this.status == StatusType.AVAILABLE) {
            name = "All Members";
        } else if (this.status == StatusType.ON_HOLD) {
            for (Reservation res : this.getReservations()) {
                name = res.getMember().getFirstName() + " " + res.getMember().getLastName();
            }
        } else if (this.status == StatusType.LOANED) {
            for (Loan loan : this.getLoans()) {
                name = loan.getMember().getFirstName() + " " + loan.getMember().getLastName();
            }
        }

        return name;
    }
}
