package com.osattarov.entities;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Odiljon Sattarov on 4/7/2015.
 */
@Entity
public class Author extends Person {
    @ElementCollection
    @CollectionTable(name = "CREDENTIALS")
    private List<String> credentials;
    @Lob
    @Column(name = "BIO")
    private String shortBio;
    @ManyToMany(mappedBy = "authors")
    private List<Book> books;

    public List<String> getCredentials() {
        return credentials;
    }

    public void setCredentials(List<String> credentials) {
        this.credentials = credentials;
    }

    public String getShortBio() {
        return shortBio;
    }

    public void setShortBio(String shortBio) {
        this.shortBio = shortBio;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
