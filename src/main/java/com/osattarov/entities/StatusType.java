package com.osattarov.entities;

/**
 * Created by Odiljon Sattarov on 4/20/2015.
 */
public enum StatusType {

    AVAILABLE(0), LOANED(1), ON_HOLD(2), PENDING(3), CANCELLED(4);

    private final int code;

    StatusType(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
