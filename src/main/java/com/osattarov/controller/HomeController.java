package com.osattarov.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Odiljon Sattarov
 */
@Controller
public class HomeController {

    @RequestMapping(value = "/")
    public String actionIndex() {
        return "redirect:member/list";
    }
}
