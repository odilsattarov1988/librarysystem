/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.osattarov.controller;

import com.osattarov.dao.GenericDao;
import com.osattarov.dto.CopyDto;
import com.osattarov.entities.Book;
import com.osattarov.entities.Copy;
import com.osattarov.entities.Magazine;
import com.osattarov.entities.Publication;
import com.osattarov.service.LibraryService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Odiljon Sattarov
 */
@Controller
@RequestMapping(value = "/publication")
public class PublicationController {

    private final String addBookUrl = "/publication/book/add";
    private final String updateBookUrl = "/publication/book/update";
    private final String addMagazineUrl = "/publication/magazine/add";
    private final String updateMagazineUrl = "/publication/magazine/update";

    @Autowired
    private GenericDao<Publication, Long> publicationDao;
    @Autowired
    private LibraryService libraryService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String getPublicationList(Model model) {
        model.addAttribute("publications", publicationDao.findAll(Publication.class));
        return "publication/list";
    }

    @RequestMapping(value = "/book/add", method = RequestMethod.GET)
    public String goToBookForm(Model model) {
        model.addAttribute("book", new Book());
        model.addAttribute("formUrl", addBookUrl);

        return "publication/addBook";
    }

    @RequestMapping(value = "/book/add", method = RequestMethod.POST)
    public String createBookForm(@Valid Book book, BindingResult result, Model model) {
        model.addAttribute("formUrl", addBookUrl);
        if (result.hasErrors()) {
            return "publication/addBook";
        }

        publicationDao.create(book);

        return "redirect:/publication/list";
    }

    @RequestMapping(value = "/magazine/add", method = RequestMethod.GET)
    public String goToMagazineForm(Model model) {
        model.addAttribute("magazine", new Magazine());
        model.addAttribute("formUrl", addMagazineUrl);

        return "publication/addMagazine";
    }

    @RequestMapping(value = "/magazine/add", method = RequestMethod.POST)
    public String createMagazineForm(@Valid Magazine magazine, BindingResult result, Model model) {
        model.addAttribute("formUrl", addMagazineUrl);
        if (result.hasErrors()) {
            return "publication/addMagazine";
        }

        publicationDao.create(magazine);

        return "redirect:/publication/list";
    }

    @RequestMapping(value = "/book/update/{bookId}", method = RequestMethod.GET)
    public String getBook(@PathVariable("bookId") long bookId, Model model) {
        model.addAttribute("book", publicationDao.findById(bookId, Publication.class));
        model.addAttribute("formUrl", updateBookUrl + "/" + bookId);
        return "publication/addBook";
    }

    @RequestMapping(value = "/book/update/{bookId}", method = RequestMethod.POST)
    public String updateBook(@Valid Book book, BindingResult result, Model model) {
        model.addAttribute("formUrl", updateBookUrl + "/" + book.getId());
        if (!result.hasErrors()) {
            publicationDao.update(book);
            return "redirect:/publication/list";
        } else {
            return "publication/addBook";
        }
    }

    @RequestMapping(value = "/magazine/update/{magazineId}", method = RequestMethod.GET)
    public String getMagazine(@PathVariable("magazineId") long magazineId, Model model) {
        model.addAttribute("magazine", publicationDao.findById(magazineId, Publication.class));
        model.addAttribute("formUrl", updateMagazineUrl + "/" + magazineId);
        return "publication/addMagazine";
    }

    @RequestMapping(value = "/magazine/update/{magazineId}", method = RequestMethod.POST)
    public String updateMagazine(@Valid Magazine magazine, BindingResult result, Model model) {
        model.addAttribute("formUrl", updateMagazineUrl + "/" + magazine.getId());
        if (!result.hasErrors()) {
            publicationDao.update(magazine);
            return "redirect:/publication/list";
        } else {
            return "publication/addMagazine";
        }
    }

    @RequestMapping(value = "/book/view/{bookId}", method = RequestMethod.GET)
    public String viewBook(@PathVariable("bookId") long bookId, Model model) {
        model.addAttribute("publication", publicationDao.findById(bookId, Publication.class));
        model.addAttribute("copy", new CopyDto());
        return "publication/view";
    }

    @RequestMapping(value = "/magazine/view/{magazineId}", method = RequestMethod.GET)
    public String viewMagazane(@PathVariable("magazineId") long magazineId, Model model) {
        model.addAttribute("publication", publicationDao.findById(magazineId, Publication.class));
        model.addAttribute("copy", new CopyDto());
        return "publication/view";
    }

    @RequestMapping(value = "/copy/add", method = RequestMethod.POST)
    public String createCopyForm(@Valid CopyDto copyDto) {
        String redirect;
        Publication publication = publicationDao.findById(copyDto.getPublicationId(), Publication.class);

        for (int i = 1; i <= copyDto.getCopyNumber(); i++) {
            Copy copy = new Copy(publication);
            libraryService.createCopy(copy);
        }

        if (publication instanceof Book) {
            redirect = "redirect:/publication/view/book/" + copyDto.getPublicationId();
        } else {
            redirect = "redirect:/publication/view/magazine/" + copyDto.getPublicationId();
        }

        return redirect;
    }

    @RequestMapping(value = "/delete/{publicationId}", method = RequestMethod.GET)
    public String deleteMember(@PathVariable("publicationId") long publicationId) {
        Publication publication = publicationDao.load(publicationId, Publication.class);
        publicationDao.delete(publication);
        return "redirect:/publication/list";
    }
}
