/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.osattarov.controller;

import com.osattarov.dao.GenericDao;
import com.osattarov.entities.Member;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Odiljon Sattarov
 */
@Controller
@RequestMapping(value = "/member")
public class MemberController {

    private final String addMemberUrl = "member/add";
    private final String updateMemberUrl = "member/update";

    @Autowired
    private GenericDao<Member, Long> memberDao;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String getMemberList(Model model) {
        model.addAttribute("members", memberDao.findAll(Member.class));
        return "member/list";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String goToMemberForm(Model model) {
        model.addAttribute("member", new Member());
        model.addAttribute("formUrl", addMemberUrl);

        return "member/add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String createMemberForm(@Valid Member member, BindingResult result, Model model) {
        model.addAttribute("formUrl", addMemberUrl);
        if (result.hasErrors()) {
            return "member/add";
        }

        memberDao.create(member);

        return "redirect:/member/list";
    }

    @RequestMapping(value = "/update/{memberId}", method = RequestMethod.GET)
    public String getMember(@PathVariable("memberId") long memberId, Model model) {
        model.addAttribute("member", memberDao.findById(memberId, Member.class));
        model.addAttribute("formUrl", updateMemberUrl + "/" + memberId);
        return "member/add";
    }

    @RequestMapping(value = "/update/{memberId}", method = RequestMethod.POST)
    public String updateMember(@Valid Member member, BindingResult result, Model model) {
        model.addAttribute("formUrl", updateMemberUrl + "/" + member.getId());
        if (!result.hasErrors()) {
            memberDao.update(member);
            return "redirect:/member/list";
        } else {
            return "member/add";
        }
    }

    @RequestMapping(value = "/view/{memberId}", method = RequestMethod.GET)
    public String viewMember(@PathVariable("memberId") long memberId, Model model) {
        model.addAttribute("member", memberDao.findById(memberId, Member.class));
        return "member/view";
    }

    @RequestMapping(value = "/delete/{memberId}", method = RequestMethod.GET)
    public String deleteMember(@PathVariable("memberId") long memberId) {
        Member member = memberDao.load(memberId, Member.class);
        memberDao.delete(member);
        return "redirect:/member/list";
    }
}
