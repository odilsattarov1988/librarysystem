/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.osattarov.controller;

import com.osattarov.dao.GenericDao;
import com.osattarov.entities.Author;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Odiljon Sattarov
 */
@Controller
@RequestMapping(value = "/author")
public class AuthorController {

    private final String addAuthorUrl = "author/add";
    private final String updateAuthorUrl = "author/update";

    @Autowired
    private GenericDao<Author, Long> authorDao;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String getAuthorList(Model model) {
        model.addAttribute("authors", authorDao.findAll(Author.class));
        return "author/list";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String goToAuthorForm(Model model) {
        model.addAttribute("author", new Author());
        model.addAttribute("formUrl", addAuthorUrl);

        return "author/add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String createAuthorForm(@Valid Author author, BindingResult result, Model model) {
        model.addAttribute("formUrl", addAuthorUrl);
        if (result.hasErrors()) {
            return "author/add";
        }

        authorDao.create(author);

        return "redirect:/author/list";
    }

    @RequestMapping(value = "/update/{authorId}", method = RequestMethod.GET)
    public String getAuthor(@PathVariable("authorId") long authorId, Model model) {
        model.addAttribute("author", authorDao.findById(authorId, Author.class));
        model.addAttribute("formUrl", updateAuthorUrl + "/" + authorId);
        return "author/add";
    }

    @RequestMapping(value = "/update/{memberId}", method = RequestMethod.POST)
    public String updateAuthor(@Valid Author author, BindingResult result, Model model) {
        model.addAttribute("formUrl", updateAuthorUrl + "" + author.getId());
        if (!result.hasErrors()) {
            authorDao.update(author);
            return "redirect:/author/list";
        } else {
            return "author/add";
        }
    }

    @RequestMapping(value = "/view/{authorId}", method = RequestMethod.GET)
    public String viewAuthor(@PathVariable("authorId") long authorId, Model model) {
        model.addAttribute("author", authorDao.findById(authorId, Author.class));
        return "author/view";
    }

    @RequestMapping(value = "/delete/{authorId}", method = RequestMethod.GET)
    public String deleteAuthor(@PathVariable("authorId") long authorId) {
        Author author = authorDao.load(authorId, Author.class);
        authorDao.delete(author);
        return "redirect:/author/list";
    }
}
