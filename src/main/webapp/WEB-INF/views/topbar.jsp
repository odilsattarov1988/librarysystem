<div class="navbar navbar-inverse navbar-static-top">
    <div class="container">
        <a href="#" class="navbar-brand"><span class="glyphicon glyphicon-home"></span>Library System</a>
        <button class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse navHeaderCollapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                <c:url value="/logout" var="logoutUrl" />
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">${pageContext.request.userPrincipal.name}<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="${logoutUrl}"><span class="glyphicon glyphicon-off"></span>Logout</a></li>
                </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
