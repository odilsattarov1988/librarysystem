<div class="col-lg-3">
    <div class="panel panel-primary">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="glyphicon glyphicon-tasks"></span>Options</div>
        <div class="panel-body">
            <div class="list-group">
                <a href="${pageContext.request.contextPath}/member/list" class="list-group-item">
                    <span class="glyphicon glyphicon-user"></span>Member management
                </a>
                <a href="${pageContext.request.contextPath}/author/list" class="list-group-item">
                    <span class="glyphicon glyphicon-user"></span>Author management
                </a>
                <a href="${pageContext.request.contextPath}/publication/list" class="list-group-item">
                    <span class="glyphicon glyphicon glyphicon-book"></span>Publication management
                </a>
            </div>
        </div>
        <div class="panel-footer">
        </div>
    </div>
</div>