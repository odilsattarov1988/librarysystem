<%-- 
    Document   : addAuthor
    Created on : Apr 20, 2015, 9:14:25 PM
    Author     : Odiljon Sattarov
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add new author | Library System</title>
        <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
        <script src="<c:url value="/resources/js/jquery-2.1.3.min.js" />" type="text/javascript" ></script>
        <script src="<c:url value="/resources/js/bootstrap.min.js" />" type="text/javascript" ></script>
    </head>
    <body>
        <%@include file="../topbar.jsp" %>
        <!-- container -->
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="panel panel-primary">
                        <!-- Default panel contents -->
                        <div class="panel-heading"><span class="glyphicon glyphicon-user"></span><span
                                class="glyphicon glyphicon-plus-sign"></span>
                                <c:if test="${author.id != 0}">
                                Edit author details
                            </c:if>

                            <c:if test="${author.id == 0}">
                                Add new author details
                            </c:if>

                        </div>
                        <div class="panel-body">
                            <form:form commandName="author" class="form-horizontal" method="POST" action="${pageContext.request.contextPath}/${formUrl}">
                                <div class="form-group">
                                    <form:label path="firstName" for="register-name" class="col-lg-2 control-label">First Name</form:label>
                                        <div class="col-lg-10">
                                        <form:input path="id" type="hidden"/>
                                        <form:input path="firstName" type="text" class="form-control" id="register-name" placeholder="First name"/>
                                        <form:errors path="firstName" cssClass="error" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="lastName" for="register-lastname" class="col-lg-2 control-label">Last Name</form:label>
                                        <div class="col-lg-10">
                                        <form:input path="lastName" class="form-control" id="register-lastname" placeholder="Last name" />
                                        <form:errors path="lastName" cssClass="error" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="email" for="register-email" class="col-lg-2 control-label">Email</form:label>
                                        <div class="col-lg-10">
                                        <form:input path="email" type="email" class="form-control" id="register-email" placeholder="you@example.com" />
                                        <form:errors path="email" cssClass="error" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="phoneNumber" for="register-phone-number" class="col-lg-2 control-label">Phone number: </form:label>
                                        <div class="col-lg-10">
                                        <form:input path="phoneNumber" type="text" class="form-control" id="register-phone-number" placeholder="Phone number" />
                                        <form:errors path="phoneNumber" cssClass="error" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <form:label path="address.street" for="register-street" class="col-lg-2 control-label">Street: </form:label>
                                        <div class="col-lg-10">
                                        <form:input path="address.street" type="text" class="form-control" id="register-street" placeholder="Street address" />
                                        <form:errors path="address.street" cssClass="error" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <form:label path="address.city" for="register-city" class="col-lg-2 control-label">City: </form:label>
                                        <div class="col-lg-10">
                                        <form:input path="address.city" type="text" class="form-control" id="register-city" placeholder="City" />
                                        <form:errors path="address.city" cssClass="error" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <form:label path="address.state" for="register-state" class="col-lg-2 control-label">State: </form:label>
                                        <div class="col-lg-10">
                                        <form:input path="address.state" class="form-control input-md" id="register-state"  maxlength="2"/>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <form:label path="address.zip" for="register-zip" class="col-lg-2 control-label">Zip Code: </form:label>
                                        <div class="col-lg-10">
                                        <form:input path="address.zip" type="text" class="form-control" id="register-zip" placeholder="Zip Code" />
                                        <form:errors path="address.zip" cssClass="error" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <form:label path="shortBio" for="register-short-bio" class="col-lg-2 control-label">Short BIO: </form:label>
                                        <div class="col-lg-10">
                                        <form:textarea path="shortBio" class="form-control" rows="3" id="register-short-bio"/>
                                        <form:errors path="shortBio" cssClass="error" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button class="btn btn-primary" type="submit" name="Save">Save</button>
                                        <a class="btn ${member.id != 0 ? 'btn-default' : 'btn-danger'}" type="submit" href="${pageContext.request.contextPath}/author/list">Cancel</a>
                                    </div>
                                </div>
                            </form:form>

                        </div>
                        <div class="panel-footer"></div>
                    </div>
                </div>
                <%@include file="../navbar.jsp" %>
            </div>
        </div>
    </body>
</html>
