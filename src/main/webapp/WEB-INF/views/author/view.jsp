<%-- 
    Document   : view
    Created on : Apr 20, 2015, 10:49:03 PM
    Author     : Odiljon Sattarov
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Member Management | Library System</title>
        <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
        <script src="<c:url value="/resources/js/jquery-2.1.3.min.js" />" type="text/javascript" ></script>
        <script src="<c:url value="/resources/js/bootstrap.min.js" />" type="text/javascript" ></script>
    </head>
</head>
<body>
    <%@include file="../topbar.jsp" %>
    <!-- container -->
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="panel panel-primary">
                    <!-- Default panel contents -->
                    <div class="panel-heading"><span class="glyphicon glyphicon-user"></span><span class="glyphicon glyphicon-search"></span>View the member details</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div>
                                <a class="btn btn-default" href="${pageContext.request.contextPath}/author/list">Back</a>
                            </div>
                        </div>
                        <%@include file="_profile.jsp" %>
                    </div>
                    <div class="panel-footer"></div>
                </div>
            </div>
            <%@include file="../navbar.jsp" %>
        </div>
    </div>
</body>
</html>
