<div class="form-horizontal">
    <div class="form-group">
        <label path="firstName" for="register-name" class="col-lg-2 control-label">First Name</label>
        <div class="col-lg-10">
            <input value="${author.firstName}" type="text" class="form-control" id="register-name" placeholder="First name" disabled/>
        </div>
    </div>
    <div class="form-group">
        <label for="register-lastname" class="col-lg-2 control-label">Last Name</label>
        <div class="col-lg-10">
            <input value="${author.lastName}" class="form-control" id="register-lastname" placeholder="Last name" disabled/>
        </div>
    </div>
    <div class="form-group">
        <label for="register-email" class="col-lg-2 control-label">Email</label>
        <div class="col-lg-10">
            <input value="${author.email}" type="email" class="form-control" id="register-email" placeholder="you@example.com" disabled/>
        </div>
    </div>
    <div class="form-group">
        <label for="register-phone-number" class="col-lg-2 control-label">Phone number: </label>
        <div class="col-lg-10">
            <input value="${author.phoneNumber}" type="text" class="form-control" id="register-phone-number" placeholder="Phone number" disabled/>
        </div>
    </div>

    <div class="form-group">
        <label for="register-street" class="col-lg-2 control-label">Street: </label>
        <div class="col-lg-10">
            <input value="${author.address.street}" type="text" class="form-control" id="register-street" placeholder="Street address" disabled/>
        </div>
    </div>

    <div class="form-group">
        <label for="register-city" class="col-lg-2 control-label">City: </label>
        <div class="col-lg-10">
            <input value="${author.address.city}" type="text" class="form-control" id="register-city" placeholder="City" disabled/>
        </div>
    </div>

    <div class="form-group">
        <label for="register-state" class="col-lg-2 control-label">State: </label>
        <div class="col-lg-10">
            <input value="${author.address.state}" class="form-control input-md" id="register-state" disabled/>
        </div>
    </div>


    <div class="form-group">
        <label for="register-zip" class="col-lg-2 control-label">Zip Code: </label>
        <div class="col-lg-10">
            <input type="text" class="form-control" id="register-zip" placeholder="Zip Code" value="${member.address.zip}" disabled />
        </div>
    </div>


    <div class="form-group">
        <label for="register-short-bio" class="col-lg-2 control-label">Short BIO: </label>
        <div class="col-lg-10">
            <textarea class="form-control input-md" rows="3" id="register-short-bio" disabled>${author.shortBio}</textarea>
        </div>
    </div>
</div>