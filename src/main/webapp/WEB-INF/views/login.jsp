<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Sign in the system | Library System</title>
        <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
    </head>
    <c:if test="${not empty custom_error}">
        <div class="error">${custom_error}</div>
    </c:if>
    <body class="register">
        <div class="container">
            <div class="panel panel-default panel-primary">
                <div class="panel-heading"><span class="glyphicon glyphicon-lock"></span>Sign in | Library System</div>
                <div class="panel-body">
                    <form class="form-signin" role="form" action="<c:url value='j_spring_security_check' />" method='POST' >
                        <h2 class="form-signin-heading">Library System</h2>
                        <input type="text" class="form-control"  name='j_username' placeholder="User Name" required autofocus />
                        <form:errors path="j_username" cssClass="error" />
                        <input type="password" class="form-control" placeholder="Password" name='j_password' required/>
                        <form:errors path="j_password" cssClass="error" />
                        <input name="submit" type="submit" value="Sign in" class="btn btn-lg btn-primary btn-block"/>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>