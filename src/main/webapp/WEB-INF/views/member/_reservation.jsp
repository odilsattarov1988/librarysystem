<div class="col-lg-15">
    <div class="panel panel-primary">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="glyphicon glyphicon-user"></span>User Reservation List</div>
        <div class="panel-body">
            <!-- Table -->
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Status Date</th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach var = "member" items = "${members}">
                    <tr>
                        <td>${member.firstName}</td>
                        <td>${member.lastName}</td>
                        <td>${member.phoneNumber}</td>
                        <td style="text-align: right;">
                            <a href="${pageContext.request.contextPath}/member/pickup/${member.id}" class="btn btn-default" data-target="tooltip" data-placement="top" title="Pickup">Pickup</a>
                            <a href="${pageContext.request.contextPath}/member/cancel/${member.id}" class="btn btn-danger">Cancel</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="panel-footer"></div>
    </div>
</div>