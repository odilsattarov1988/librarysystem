<div class="col-lg-15">
    <div class="panel panel-primary">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="glyphicon glyphicon-user"></span>User Loan List</div>
        <div class="panel-body">
            <!-- Table -->
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Checkout Date</th>
                        <th>Return Date</th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach var = "member" items = "${members}">
                    <tr>
                        <td>${member.firstName}</td>
                        <td>${member.lastName}</td>
                        <td>${member.email}</td>
                        <td style="text-align: right;">
                            <a href="${pageContext.request.contextPath}/member/return/${member.id}" class="btn btn-default" data-target="tooltip" data-placement="top" title="Edit"><span class="glyphicon glyphicon-pencil"></span>Return</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="panel-footer"></div>
    </div>
</div>