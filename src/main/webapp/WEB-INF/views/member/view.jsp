<%-- 
    Document   : list
    Created on : Apr 20, 2015, 10:49:03 PM
    Author     : Odiljon Sattarov
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Member Management | Library System</title>
        <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
        <script src="<c:url value="/resources/js/jquery-2.1.3.min.js" />" type="text/javascript" ></script>
        <script src="<c:url value="/resources/js/bootstrap.min.js" />" type="text/javascript" ></script>
    </head>
</head>
<body>
    <%@include file="../topbar.jsp" %>
    <!-- container -->
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="panel panel-primary">
                    <!-- Default panel contents -->
                    <div class="panel-heading"><span class="glyphicon glyphicon-user"></span><span class="glyphicon glyphicon-search"></span>View the member details</div>
                    <div class="panel-body">

                        <div class="form-group">
                            <div>
                                <a class="btn btn-default" href="${pageContext.request.contextPath}/member/list">Back</a>
                            </div>
                        </div>
                        <div role="tabpanel">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#member-info" aria-controls="member-info" role="tab" data-toggle="tab">Member Info</a></li>
                                <li role="presentation"><a href="#reservation" aria-controls="reservation" role="tab" data-toggle="tab">Reservation</a></li>
                                <li role="presentation"><a href="#loans" aria-controls="loan" role="tab" data-toggle="tab">Loans</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="member-info"><%@include file="_profile.jsp" %></div>
                                <div role="tabpanel" class="tab-pane" id="reservation"><%@include file="_reservation.jsp" %></div>
                                <div role="tabpanel" class="tab-pane" id="loans"><%@include file="_loan.jsp" %></div>
                            </div>

                        </div>

                    </div>
                    <div class="panel-footer"></div>
                </div>
            </div>
            <%@include file="../navbar.jsp" %>
        </div>
    </div>
</body>
</html>
