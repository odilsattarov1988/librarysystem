<%-- 
    Document   : list
    Created on : Apr 20, 2015, 10:49:03 PM
    Author     : Odiljon Sattarov
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Publication Management | Library System</title>
        <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
        <script src="<c:url value="/resources/js/jquery-2.1.3.min.js" />" type="text/javascript" ></script>
        <script src="<c:url value="/resources/js/bootstrap.min.js" />" type="text/javascript" ></script>
    </head>
</head>
<body>
    <%@include file="../topbar.jsp" %>
    <!-- container -->
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="panel panel-primary">
                    <!-- Default panel contents -->
                    <div class="panel-heading"><span class="glyphicon glyphicon-user"></span>Publication Management</div>
                    <div class="panel-body">
                        <a href="book/add" class="btn btn-default"><span class="glyphicon glyphicon-plus-sign"></span>Add
                            new book</a>
                        <a href="magazine/add" class="btn btn-default"><span class="glyphicon glyphicon-plus-sign"></span>Add
                            new magazine</a>
                        <!-- Table -->
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Copies</th>
                                    <th>Available Copies</th>
                                    <th>Operations</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var = "publication" items = "${publications}">
                                    <tr>
                                        <td>${publication.title}</td>
                                        <td>${publication.copies.size()}</td>
                                        <td>${publication.getAvailableCopies()}</td>
                                        <td style="text-align: right;">
                                            <a href="${pageContext.request.contextPath}/publication/${publication.getClass().getSimpleName().toLowerCase()}/update/${publication.id}" class="btn btn-default" data-target="tooltip" data-placement="top" title="Edit"><span class="glyphicon glyphicon-pencil"></span>Edit</a>
                                            <a href="${pageContext.request.contextPath}/publication/${publication.getClass().getSimpleName().toLowerCase()}/view/${publication.id}" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>View Publication</a>
                                            <a href="${pageContext.request.contextPath}/publication/delete/${publication.id}" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span>Delete</a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer"></div>
                </div>
            </div>

            <%@include file="../navbar.jsp" %>
        </div>
    </div>
</body>
</html>
