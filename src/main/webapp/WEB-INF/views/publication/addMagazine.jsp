<%-- 
    Document   : add publication
    Created on : Apr 20, 2015, 9:14:25 PM
    Author     : Odiljon Sattarov
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add new publication | Library System</title>
        <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
        <script src="<c:url value="/resources/js/jquery-2.1.3.min.js" />" type="text/javascript" ></script>
        <script src="<c:url value="/resources/js/bootstrap.min.js" />" type="text/javascript" ></script>
    </head>
    <body>
        <%@include file="../topbar.jsp" %>
        <!-- container -->
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="panel panel-primary">
                        <!-- Default panel contents -->
                        <div class="panel-heading"><span class="glyphicon glyphicon-user"></span><span
                                class="glyphicon glyphicon-plus-sign"></span>
                            Publication details
                        </div>
                        <div class="panel-body">
                            <form:form commandName="magazine" class="form-horizontal" method="POST" action="${pageContext.request.contextPath}${formUrl}">
                                <div class="form-group">
                                    <form:label path="title" for="register-title" class="col-lg-2 control-label">Title:</form:label>
                                        <div class="col-lg-10">
                                        <form:input path="id" type="hidden"/>
                                        <form:input path="title" type="text" class="form-control" id="register-title" placeholder="Title"/>
                                        <form:errors path="title" cssClass="error" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="issueNumber" for="register-issueNumber" class="col-lg-2 control-label">Issue Number:</form:label>
                                        <div class="col-lg-10">
                                        <form:input path="issueNumber" class="form-control" id="register-issueNumber" placeholder="Issue Number" />
                                        <form:errors path="issueNumber" cssClass="error" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <form:label path="maxCheckoutLength" for="register-maxCheckoutLength" class="col-lg-2 control-label">Max CheckOut Days:</form:label>
                                        <div class="col-lg-10">
                                        <form:input path="maxCheckoutLength" type="maxCheckoutLength" class="form-control" id="register-maxCheckoutLength" placeholder="Only number" />
                                        <form:errors path="maxCheckoutLength" cssClass="error" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button class="btn btn-primary" type="submit" name="Save">Save</button>
                                        <a class="btn ${magazine.id != 0 ? 'btn-default' : 'btn-danger'}" type="submit" href="${pageContext.request.contextPath}/publication/list">Cancel</a>
                                    </div>
                                </div>
                            </form:form>
                        </div>
                        <div class="panel-footer"></div>
                    </div>
                </div>
                <%@include file="../navbar.jsp" %>
            </div>
        </div>
    </body>
</html>