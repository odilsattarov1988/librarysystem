<a href="#add-copy" class="btn btn-default" data-target="#add-copy" data-toggle="modal"><span class="glyphicon glyphicon-plus-sign"></span>Add new copy</a>
<table class="table table-hover">
    <thead>
        <tr>
            <th>Copy Number</th>
            <th>Status</th>
            <th>For</th>
        </tr>
    </thead>
    <tbody>
    <c:forEach var = "copy" items = "${publication.copies}">
        <tr>
            <td>${copy.id}</td>
            <td>${copy.status}</td>
            <td>${copy.getMemberName()}</td>
        </tr>
    </c:forEach>
</tbody>
</table>

<div class="modal fade" id="add-copy" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Add Copy</h4>
            </div>
            <div class="modal-body">
                <form:form id="copyForm" commandName="copy" class="form-horizontal" method="POST" action="${pageContext.request.contextPath}/publication/copy/add">
                    <div class="form-group">
                        <input type="hidden" value="${publication.id}" id="publicationId" name="publicationId"/>
                        <form:label path="copyNumber" id="copyNumber" for="register-copyNumber" class="col-lg-3 control-label">Copy Number:</form:label>
                        <div class="col-lg-5">
                            <form:input path="copyNumber" type="text" class="form-control" id="register-copyNumber" placeholder="Copy Number"/>
                            <form:errors path="copyNumber" cssClass="error" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-5">
                            <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                            <button class="btn btn-primary" type="submit" name="Save">Save</button>
                        </div>
                    </div>
                </form:form>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>