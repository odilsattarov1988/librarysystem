<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
    <div class="col-md-10">
        <div class="panel panel-info">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-book"></span>Publication Info
            </div>
            <div class="panel-body">
                <table class="table table-hover">
                    <tr>
                        <td class="text-muted">Title:</td>
                        <td>${publication.title}</td>
                        <c:if test="${publication.getClass().getSimpleName() eq 'Book'}">
                            <td class="text-muted">ISBN:</td>
                            <td>${publication.ISBN}</td>
                        </c:if>
                        <c:if test="${publication.getClass().getSimpleName() eq 'Magazine'}">
                            <td class="text-muted">Issue Number:</td>
                            <td>${publication.issueNumber}</td>
                        </c:if>
                    </tr>
                    <tr><td class="text-muted">Max CheckOut duration:</td><td>${publication.maxCheckoutLength}</td><td class="text-muted">Copies:</td><td>${publication.copies.size()}</td></tr>
                    <tr><td class="text-muted">Available Copies</td><td>${publication.getAvailableCopies()} </td><td class="text-muted">Hold Copies:</td><td>${publication.getOnHoldReservation()}</td></tr>

                </table>
            </div>
        </div>
    </div>
</div>