<%-- 
    Document   : list
    Created on : Apr 20, 2015, 10:49:03 PM
    Author     : Odiljon Sattarov
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Publication Management | Library System</title>
        <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
        <script src="<c:url value="/resources/js/jquery-2.1.3.min.js" />" type="text/javascript" ></script>
        <script src="<c:url value="/resources/js/bootstrap.min.js" />" type="text/javascript" ></script>
        <script src="<c:url value="/resources/js/custom.js" />" type="text/javascript" ></script>
    </head>
</head>
<body>
    <%@include file="../topbar.jsp" %>
    <!-- container -->
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="panel panel-primary">
                    <!-- Default panel contents -->
                    <div class="panel-heading"><span class="glyphicon glyphicon-book"></span><span class="glyphicon glyphicon-search"></span>View the publication details</div>
                    <div class="panel-body">

                        <div class="form-group">
                            <div>
                                <a class="btn btn-default" href="${pageContext.request.contextPath}/publication/list">Back</a>
                            </div>
                        </div>
                        <div role="tabpanel">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#publication-info" aria-controls="publication-info" role="tab" data-toggle="tab">Publication Info</a></li>
                                <li role="presentation"><a href="#copies" aria-controls="reservation" role="tab" data-toggle="tab">Copies</a></li>
                                <li role="presentation"><a href="#reservations" aria-controls="loan" role="tab" data-toggle="tab">Reservations</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="publication-info"><%@include file="_info.jsp" %></div>
                                <div role="tabpanel" class="tab-pane" id="copies"><%@include file="_copies.jsp" %></div>
                                <div role="tabpanel" class="tab-pane" id="reservations"><%@include file="_reservation.jsp" %></div>
                            </div>

                        </div>

                    </div>
                    <div class="panel-footer"></div>
                </div>
            </div>
            <%@include file="../navbar.jsp" %>
        </div>
    </div>
</body>
</html>
